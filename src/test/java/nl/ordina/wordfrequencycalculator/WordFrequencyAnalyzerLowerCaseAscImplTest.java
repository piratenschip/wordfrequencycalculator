package nl.ordina.wordfrequencycalculator;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WordFrequencyAnalyzerLowerCaseAscImplTest {

	private static final WordFrequencyAnalyzer wordFrequencyAnalyzer = new WordFrequencyAnalyzerLowerCaseAscImpl();

	@Test
	public void calculateFrequencyForWordTestDifferentSeparators() {
		// Given
		final String givenText = "This texT contains the word TExt 2 times, but that's not tHe word that is used THE most.";
		final String givenWord = "teXT";

		// When
		final int foundFrequency = wordFrequencyAnalyzer.calculateFrequencyForWord(givenText, givenWord);

		// Then
		assertEquals(2, foundFrequency, "The word 'text' should be found twice within the given text!");
	}

	@Test
	public void calculateFrequencyForWordTestFindFrequencySuccesfull() {
		// Given
		final String givenText = "This texT contains the word TExt twice";
		final String givenWord = "teXT";

		// When
		final int foundFrequency = wordFrequencyAnalyzer.calculateFrequencyForWord(givenText, givenWord);

		// Then
		assertEquals(2, foundFrequency, "The word 'text' should be found twice within the given text!");
	}

	@Test
	public void calculateHighestFrequencyTestMultipleWordsWithTheHighestFrequency() {
		// Given
		final String givenText = "The Case is that tHE cAse appears twice!";

		// When
		final int foundHighestFrequency = wordFrequencyAnalyzer.calculateHighestFrequency(givenText);

		// Then
		assertEquals(2, foundHighestFrequency); // Expecting 'the' and 'case' to be found the most.
	}

	@Test
	public void calculateHighestFrequencyTestOneWordWithTheHighestFrequency() {
		// Given
		final String givenText = "Random text with the word THE appearing thrice in tHe string";

		// When
		final int foundHighestFrequency = wordFrequencyAnalyzer.calculateHighestFrequency(givenText);

		// Then
		assertEquals(3, foundHighestFrequency); // Expecting 'the' to be found the most.
	}

	@Test
	public void calculateHighestFrequencyTestOnlySeparatorsShouldReturnZero() {
		// Given
		final String givenText = "0_ -1 3 9";

		// When
		final int foundHighestFrequency = wordFrequencyAnalyzer.calculateHighestFrequency(givenText);

		// Then
		assertEquals(0, foundHighestFrequency, "Expecting 0 as only separators were given.");
	}

	@Test
	public void calculateHighestFrequencyTestZeroCharacterWordsNotCountedAsHighestFrequency() {
		// Given
		final String givenText = "This texT contains the word TExt 2 _ times, but that's not tHe word _ that is used THE most.";

		// When
		final int foundHighestFrequency = wordFrequencyAnalyzer.calculateHighestFrequency(givenText);

		// Then
		assertEquals(3, foundHighestFrequency); // Expecting 'the' to be found the most.
	}

	@Test
	public void calculateMostFrequentNWordsTestNBiggerThanTotalWords() {
		// Given
		final String givenText = "cASE CASE The Case tHE";
		final int givenNumberOfWords = 6; // GivenText only contains 2 words

		// When
		final List<WordFrequency> foundWordFrequencies = wordFrequencyAnalyzer.calculateMostFrequentNWords(givenText, givenNumberOfWords);

		// Then
		assertEquals(2, foundWordFrequencies.size()); // Expecting the numberOfWords to be working
		assertEquals("case", foundWordFrequencies.get(0).getWord());
		assertEquals(3, foundWordFrequencies.get(0).getFrequency());
		assertEquals("the", foundWordFrequencies.get(1).getWord());
		assertEquals(2, foundWordFrequencies.get(1).getFrequency());
	}

	@Test
	public void calculateMostFrequentNWordsTestReturnSubset() {
		// Given
		final String givenText = "The Case is that tHE cAse appears twice!";
		final int givenNumberOfWords = 2;

		// When
		final List<WordFrequency> foundWordFrequencies = wordFrequencyAnalyzer.calculateMostFrequentNWords(givenText, givenNumberOfWords);

		// Then
		assertEquals(2, foundWordFrequencies.size()); // Expecting the numberOfWords to be working
		assertEquals("case", foundWordFrequencies.get(0).getWord());
		assertEquals(2, foundWordFrequencies.get(0).getFrequency());
		assertEquals("the", foundWordFrequencies.get(1).getWord());
		assertEquals(2, foundWordFrequencies.get(1).getFrequency());
	}
}