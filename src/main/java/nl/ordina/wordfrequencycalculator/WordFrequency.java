package nl.ordina.wordfrequencycalculator;

/**
 * Interface to be used to return a word and its frequency within a given text.
 */
public interface WordFrequency {

	int getFrequency();

	String getWord();
}
