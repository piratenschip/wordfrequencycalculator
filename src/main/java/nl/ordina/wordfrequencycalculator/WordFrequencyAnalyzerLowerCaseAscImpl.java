package nl.ordina.wordfrequencycalculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;

/**
 * Implementation of the {@linkplain WordFrequencyAnalyzer} interface which calculates based on lowercasing the input
 * and returns the calculations lowercase and in ascendant alphabetical order.
 */
public class WordFrequencyAnalyzerLowerCaseAscImpl implements WordFrequencyAnalyzer {

	// Anything not alphabetic is treated as a separator.
	private static final String SEPARATOR_REGEX = "[^a-zA-Z]";

	/**
	 * Calculates the frequency of the given word within the given text. Anything non-alphabetic is treated as a separator.
	 * Matches to the given word are done based on lowercase.
	 *
	 * @param text Text to find the frequency of a given word in.
	 * @param word Word to find the frequency of in a given text.
	 * @return Frequency of the given word in the given text.
	 */
	@Override
	public int calculateFrequencyForWord(String text, String word) {
		Objects.requireNonNull(text, "Need a text to calculate the frequency of the given word!");
		Objects.requireNonNull(word, "Need a word to calculate its frequency within the given text!");

		word = word.toLowerCase();

		return (int) Arrays.stream(text.split(SEPARATOR_REGEX))
				.map(String::toLowerCase) // words within text are mapped to lowercase
				.filter(word::equals) // We're only interested in words that match our searchword.
				.count();
	}

	@Override
	public int calculateHighestFrequency(String text) {
		Objects.requireNonNull(text, "Need a text to calculate the highest frequency of a word within!");

		Map<String, Integer> frequencyPerWord = getFrequencyPerWordMap(text);
		return frequencyPerWord.values().stream()
				.max(Integer::compareTo)
				.orElse(0); // If a String with only separators has been given, no words were found thus the highest frequency is 0.
	}

	@Override
	public List<WordFrequency> calculateMostFrequentNWords(String text, int n) {

		TreeSet<WordFrequency> wordFrequencies = new TreeSet<>(getWordFrequencyComparator());

		Map<String, Integer> frequencyPerWord = getFrequencyPerWordMap(text);

		frequencyPerWord.forEach((key, value) -> wordFrequencies.add(new WordFrequency() {
			@Override
			public int getFrequency() {
				return value;
			}

			@Override
			public String getWord() {
				return key;
			}

			@Override
			public String toString() {
				return "(\"" + getWord() + "\", " + getFrequency() + ")";
			}
		}));

		List<WordFrequency> orderListWithWordFrequencies = new ArrayList<>(wordFrequencies);
		if (n > orderListWithWordFrequencies.size()) {
			n = orderListWithWordFrequencies.size();
		}
		return orderListWithWordFrequencies.subList(0, n);
	}

	private Map<String, Integer> getFrequencyPerWordMap(String text) {
		Map<String, Integer> frequencyPerWord = new HashMap<>();

		Arrays.stream(text.split(SEPARATOR_REGEX))
				.filter(word -> !word.isEmpty()) // A word is defined as one or more alphabetical characters, so filter out empty Strings.
				.map(String::toLowerCase) // words within text are mapped to lowercase
				.forEach(word -> frequencyPerWord.compute(word, (key, currentFrequency) -> currentFrequency == null ? 1 : ++currentFrequency)); // If the word is found for the first time use 1, otherwise increment by 1.

		return frequencyPerWord;
	}

	/**
	 * Comparator for {@linkplain WordFrequency}.
	 * Compares based on frequency first, afterwards compares the word alphabetically.
	 *
	 * @return Comparator based on frequency and word alphabetical.
	 */
	private Comparator<WordFrequency> getWordFrequencyComparator() {
		return (wordFrequency1, wordFrequency2) -> {

			if (wordFrequency1.getFrequency() > wordFrequency2.getFrequency()) {
				return -1;
			}
			if (wordFrequency1.getFrequency() < wordFrequency2.getFrequency()) {
				return 1;
			}

			return wordFrequency1.getWord().compareToIgnoreCase(wordFrequency2.getWord());
		};
	}
}
