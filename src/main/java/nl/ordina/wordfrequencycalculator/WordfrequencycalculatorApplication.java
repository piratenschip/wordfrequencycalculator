package nl.ordina.wordfrequencycalculator;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class WordfrequencycalculatorApplication {

	private static final WordFrequencyAnalyzer wordFrequencyAnalyzer = new WordFrequencyAnalyzerLowerCaseAscImpl();

	public static void main(String[] args) {
		if (args.length != 2 && args.length != 3) {
			throw new IllegalArgumentException("Should've gotten 2 or 3 arguments; java CountingServiceApplication <calculateFrequencyForWord|calculateHighestFrequency|calculateMostFrequentNWords> \"text with whatever in between\" <word or number>");
		}

		switch (args[0]) {
			case "calculateFrequencyForWord":
				if (args.length != 3) {
					throw new IllegalArgumentException("calculateFrequencyForWord expects 2 arguments; 'text' and 'word'");
				}

				System.out.println("Calculated Frequency for word: " + wordFrequencyAnalyzer.calculateFrequencyForWord(args[1], args[2]));
				break;
			case "calculateHighestFrequency":
				if (args.length != 2) {
					throw new IllegalArgumentException("calculateHighestFrequency expects 1 arguments; 'text'");
				}

				System.out.println("Calculated Highest Frequency is: " + wordFrequencyAnalyzer.calculateHighestFrequency(args[1]));
				break;
			case "calculateMostFrequentNWords":
				if (args.length != 3 || !StringUtils.isNumeric(args[2])) {
					throw new IllegalArgumentException("calculateMostFrequentNWords expects 2 arguments 'text' and 'n', of which 'n' should be a number");
				}

				List<WordFrequency> foundFrequencies = wordFrequencyAnalyzer.calculateMostFrequentNWords(args[1], Integer.parseInt(args[2]));
				System.out.println("The following words had the highest frequency: " + foundFrequencies);
				break;
			default:
				throw new IllegalArgumentException(String.format("%s is an unknown method!", args[0]));
		}
	}
}
