package nl.ordina.wordfrequencycalculator;

import java.util.List;

/**
 * Analyzer interface for word frequency within a given text.
 */
public interface WordFrequencyAnalyzer {

	int calculateFrequencyForWord(String text, String word);

	int calculateHighestFrequency(String text);

	List<WordFrequency> calculateMostFrequentNWords(String text, int n);

}
