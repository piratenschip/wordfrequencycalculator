# Getting Started
This countingservice has 3 methods that can be called:
- calculateFrequencyForWord
- calculateHighestFrequency
- calculateMostFrequentNWords

These have to be called using the command line. It is assumed you know where to find the useable jar. For the examples below 
we assume you're in the folder with the useable .jar and that it's called "countingservice-core-0.0.1-SNAPSHOT.jar".

## calculateFrequencyForWord ##
This method calculates the frequency within a given text for the given word. Thus it needs 2 arguments besides the one used 
to decide which method to call.  
An example is as follows;  
`java -jar wordfrequencycalculator-0.0.1-SNAPSHOT.jar calculateFrequencyForWord "thiS text has ThIS twice" this`

Output for this command is as follows:  
`Calculated Frequency for word: 2`

## calculateHighestFrequency ##
This method calculates what the highest frequency of the words within a given text is. Thus it needs only a single 
argument besides the one used to decide which method to call.  
An example is as follows;  
`java -jar wordfrequencycalculator-0.0.1-SNAPSHOT.jar calculateHighestFrequency "thiS is ThIS"`

Output for this command is as follows:  
`Calculated Highest Frequency is: 2`

## calculateMostFrequentNWords ##
This method calculates the N words with the highest frequency within a given text. In case multiple words have the same 
frequency they're ordered based on the word alphabetically. It needs 2 arguments besides the one used to decide which method to call.  
An example is as follows;  
`java -jar wordfrequencycalculator-0.0.1-SNAPSHOT.jar calculateMostFrequentNWords "thiS text has ThIS twice" 2`

Output for this command is as follows:  
`The following words had the highest frequency: [("this", 2), ("has", 1)]`